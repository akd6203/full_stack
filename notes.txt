HTML Tags

1. Singular Tags - <br> <hr>
2. Paired Tags  - <html>gjh</html>
3. Self closing Tags    - <br/> HTML5



HTML Elements:
1. Inline Level Elements 
    takes space according to its content, Eg. strong,em etc.
2. Block Level Elements
    acquires 100% of the screen, eg. h1,p etc.


TYPES OF CSS
1. Inline CSS -- within tag --style as an attribute
2. Internal CSS -- within head tag  -- style as an tag
3. External CSS -- outside html file in .css file

Syntax
    prop-name:value;prop-name:value;


text-shadow:horizontal vertical blurRate color;
box-shadow:horizontal vertical blurRate color;
letter-spacing,word-spacing,

text-decoration: none | underline | overline | line-through
text-transform: uppercase | lowercase | capitalize



HTML LIST
1. Ordered LIST
    type - Numbers (1)
           Uppercase Alphabets (A)
           LOwercase Alphabets (a)
           Uppercase Romans(I) 
           Lowercase Romans(i) 
2. Unordered List Types:

    disc | square | circle | none


EMOJIPEDIA - https://emojipedia.org/
2. Search for emoji
3. Select emoji
4. Copy + Paste


CSS Selectrors:

1. By Tag name (h1)
2. Class name (.classname)  -- can be common for multiple Elements
3. ID Name  (#idname)   --ID name should be unique

HTML Images
    <img src="IMAGE PATH.ext" alt="ALternate Text">

CSS Background
    background-image:url("path.ext");

CSS - border-radius, text-align:justify,


HTML Table

<marquee direction="" behaviour="" loop="" scrollamount=""></marquee>
    direction - left | right | up| down
    behaviour - scroll | slide | alternate